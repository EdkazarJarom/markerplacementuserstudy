﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.VR.WSA;

public class MarkerPlacement : MonoBehaviour
{
    public bool isActive = true;

    public int NumTrials = 3;
	public int NumMarkersPerTrial = 7;

    private float transAmount = 0.025f;
    private float rotAmount = 2.5f;

	private List<GameObject> TrialContainers;

	private List<List<GameObject>> MarkerObjects;

    private int currentTrial;
	private int currentMarker;

    private GameObject mainContainer;

	private Transform MannequinMeshContainer;

    // Use this for initialization
    void Start ()
    {
        if (mainContainer == null)
        {
            mainContainer = GameObject.Find("MarkerPlacementExperiment");
            if (mainContainer == null)
            {
                Debug.LogError("could not find root model");
            }
        }

        if (MannequinMeshContainer == null) {
			MannequinMeshContainer = transform.Find ("Mannequin Mesh");
			if (MannequinMeshContainer == null) {
				Debug.LogError ("could not find mannequin mesh");
			}
		}

		TrialContainers = new List<GameObject> ();
		MarkerObjects = new List<List<GameObject>> ();

		for (int trialIdx = 1; trialIdx <= NumTrials; trialIdx++) {
			Transform trialContainer = transform.Find ("Task 1 Trial " + trialIdx);
			if (trialContainer == null) {
				Debug.LogError ("Could not find container object for trial " + trialIdx);
			} else {
				TrialContainers.Add (trialContainer.gameObject);

				List<GameObject> markerObjectsForThisTrial = new List<GameObject> ();

				for (int markerIdx = 1; markerIdx <= NumMarkersPerTrial; markerIdx++) {
					Transform markerObject = trialContainer.Find ("CircleAnnotation " + markerIdx);

					if (markerObject == null) {
						Debug.LogError ("Could not find marker " + markerIdx + " in trial " + trialIdx);
					} else {
						markerObjectsForThisTrial.Add (markerObject.gameObject);
					}
				}

				MarkerObjects.Add (markerObjectsForThisTrial);
			}
		}

        HideMannequinMesh ();

        ResetExperiment ();
	}

    // Update is called once per frame
    void Update()
    {
        //Switch active model
        if (Input.GetKeyDown(KeyCode.S))
        {
            changeActiveState();
        }
        if (isActive)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Advance();
            }
            else if (Input.GetKeyDown(KeyCode.Backspace))
            {
                InitTrial(currentTrial);
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                ResetExperiment();
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                ToggleMannequinMesh();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                TranslateModel(0.0f, transAmount, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                TranslateModel(0.0f, -transAmount, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                TranslateModel(transAmount, 0.0f, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                TranslateModel(-transAmount, 0.0f, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.P))//Push
            {
                TranslateModel(0.0f, 0.0f, transAmount);
            }
            else if (Input.GetKeyDown(KeyCode.L))//Pull
            {
                TranslateModel(0.0f, 0.0f, -transAmount);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                RotateModel(rotAmount);
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                RotateModel(-rotAmount);
            }
        }

        updateFocus();
    }

    public void changeActiveState()
    {
        if (isActive)
        {
            HideMannequinMesh();

            ResetExperiment();
        }
        isActive = !isActive;
    }

    public void ToggleMannequinMesh()
    {
		MannequinMeshContainer.gameObject.SetActive (!MannequinMeshContainer.gameObject.activeSelf);
	}

	public void InitTrial(int trialNumber) {
		currentTrial = trialNumber;

		// hide all other trials
		for (int trialIdx = 1; trialIdx <= NumTrials; trialIdx++) {
			GameObject trialContainer = TrialContainers [trialIdx - 1];
			trialContainer.SetActive (trialIdx == currentTrial);
		}

		SetToMarker (0);
	}

	public void Advance() {
		int nextMarkerNumber = currentMarker + 1;
		if (nextMarkerNumber > NumMarkersPerTrial) {
			int nextTrialNumber = currentTrial + 1;
			if (nextTrialNumber > NumTrials) {
				ResetExperiment ();
			} else {
				InitTrial (nextTrialNumber);
			}
		} else {
			SetToMarker (nextMarkerNumber);
		}
	}

    public void TranslateModel(float transX,float transY,float transZ)
    {
        Transform mainContainerTransform =  mainContainer.GetComponent<Transform>();
        mainContainerTransform.position = new Vector3(mainContainerTransform.position.x + transX, mainContainerTransform.position.y + transY, mainContainerTransform.position.z + transZ);
    }

    public void RotateModel(float angle)
    {
        Transform mainContainerTransform = mainContainer.GetComponent<Transform>();
        mainContainerTransform.Rotate(0.0f, angle, 0.0f);
    }

    public int getCurrentTrail()
    {
        return currentTrial;
    }

   public void ResetExperiment()
    {
		InitTrial (1);
	}	

    void HideMannequinMesh()
    {
        MannequinMeshContainer.gameObject.SetActive(false);
    }

    void SetToMarker(int markerNumber)
    {
        currentMarker = markerNumber;

        List<GameObject> markerObjectsForThisTrial = MarkerObjects[currentTrial - 1];

        for (int markerIdx = 1; markerIdx <= NumMarkersPerTrial; markerIdx++)
        {
            GameObject markerObject = markerObjectsForThisTrial[markerIdx - 1];
            markerObject.SetActive(markerIdx <= currentMarker);
        }
    }

    private void updateFocus()
    {
        var normal = -Camera.main.transform.forward;

        for (int trailID = 0; trailID <= NumTrials; trailID++)
        {
            List<GameObject> currentMarkers = MarkerObjects[trailID];

            for(int markerID = 0; markerID<NumMarkersPerTrial;markerID++)
            {
                GameObject focusedObject = currentMarkers[markerID];
                var position = focusedObject.transform.position;
                HolographicSettings.SetFocusPointForFrame(position, normal);
            }
            
        }
    }
}
