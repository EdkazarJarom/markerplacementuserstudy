﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class SpeechManager : MonoBehaviour
{
    private float transAmount = 0.025f;
    private float rotAmount = 2.5f;
    private bool isMarkerExperiment = true;

    private KeywordRecognizer _keywordRecognizer = null;
    private readonly Dictionary<string, System.Action> _keywords = new Dictionary<string, System.Action>();

    MarkerPlacement markerPlacementScript;
    AbdominalIncision abdominalIncisionScript;

    // Use this for initialization
    private void Start ()
    {
        // Experiment 
        _keywords.Add("Quit", quitApplication);

        // Experiment 
        _keywords.Add("Change Experiment", setActiveExperiment);

        // Flow Control
        _keywords.Add("Next", nextExperimentStep);
        _keywords.Add("Mesh", toggleMesh);
        _keywords.Add("Reset", resetCurrent);
        _keywords.Add("Reset All", resetExperiment);

        // Translations
        _keywords.Add("Translate Up", translateUp);
        _keywords.Add("Translate Down", translateDown);
        _keywords.Add("Translate Right", translateRight);
        _keywords.Add("Translate Left", translateLeft);
        _keywords.Add("Translate Pull", translatePull);
        _keywords.Add("Translate Push", translatePush);
        _keywords.Add("Rotate Clockwise", yawRotateClock);
        _keywords.Add("Rotate Counterclockwise", yawRotateCntrClock);

        _keywordRecognizer = new KeywordRecognizer(_keywords.Keys.ToArray());
        _keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        _keywordRecognizer.Start();

        GameObject placementContainer = GameObject.Find("MarkerPlacementExperiment");
        GameObject incisionContainer = GameObject.Find("AbdominalIncisionExperiment");
        markerPlacementScript = placementContainer.GetComponent<MarkerPlacement>();
        abdominalIncisionScript = incisionContainer.GetComponent<AbdominalIncision>();
    }

    private void quitApplication()
    {
        Application.Quit();
    }

    private void setActiveExperiment()
    {
        isMarkerExperiment = !isMarkerExperiment;
        markerPlacementScript.changeActiveState();
        abdominalIncisionScript.changeActiveState();
    }

    private void nextExperimentStep()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.Advance();
        }
        else
        {
            abdominalIncisionScript.Advance();
        }
    }

    private void toggleMesh()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.ToggleMannequinMesh();
        }
        else
        {
            abdominalIncisionScript.ToggleMannequinMesh();
        }
    }

    private void resetCurrent()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.InitTrial(markerPlacementScript.getCurrentTrail());
        }
    }

    private void resetExperiment()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.ResetExperiment();
        }
        else
        {
            abdominalIncisionScript.ResetExperiment();
        }
    }


    private void translateUp()
    {
        markerPlacementScript.TranslateModel(0.0f, transAmount, 0.0f);
        abdominalIncisionScript.TranslateModel(0.0f, transAmount, 0.0f);
    }

    private void translateDown()
    {
        markerPlacementScript.TranslateModel(0.0f, -transAmount, 0.0f);
        abdominalIncisionScript.TranslateModel(0.0f, -transAmount, 0.0f);
    }

    private void translateRight()
    {
        markerPlacementScript.TranslateModel(transAmount, 0.0f, 0.0f);
        abdominalIncisionScript.TranslateModel(transAmount, 0.0f, 0.0f);
    }

    private void translateLeft()
    {
        markerPlacementScript.TranslateModel(-transAmount, 0.0f, 0.0f);
        abdominalIncisionScript.TranslateModel(-transAmount, 0.0f, 0.0f);
    }

    private void translatePull()
    {
        markerPlacementScript.TranslateModel(0.0f, 0.0f, -transAmount);
        abdominalIncisionScript.TranslateModel(0.0f, 0.0f, -transAmount);
    }

    private void translatePush()
    {
        markerPlacementScript.TranslateModel(0.0f, 0.0f, transAmount);
        abdominalIncisionScript.TranslateModel(0.0f, 0.0f, transAmount);
    }

    private void yawRotateClock()
    {
        markerPlacementScript.RotateModel(rotAmount);
        abdominalIncisionScript.RotateModel(rotAmount);
    }

    private void yawRotateCntrClock()
    {
        markerPlacementScript.RotateModel(-rotAmount);
        abdominalIncisionScript.RotateModel(-rotAmount);
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (_keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }
}
