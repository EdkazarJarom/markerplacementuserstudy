﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloLensXboxController;

public class XboxControllerManager : MonoBehaviour
{
    private ControllerInput controllerInput;

    private bool isMarkerExperiment = true;
    private float transAmount = 0.005f;
    private float rotAmount = 0.5f;

    MarkerPlacement markerPlacementScript;
    AbdominalIncision abdominalIncisionScript;

    // Use this for initialization
    void Start ()
    {
        controllerInput = new ControllerInput(0, 0.19f);

        GameObject placementContainer = GameObject.Find("MarkerPlacementExperiment");
        GameObject incisionContainer = GameObject.Find("AbdominalIncisionExperiment");
        markerPlacementScript = placementContainer.GetComponent<MarkerPlacement>();
        abdominalIncisionScript = incisionContainer.GetComponent<AbdominalIncision>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        controllerInput.Update();

        controllerUpdate();
    }

    void controllerUpdate()
    {
        if (controllerInput.GetButtonDown(ControllerButton.Y))
        {
            setActiveExperiment();
        }

        if (controllerInput.GetButtonDown(ControllerButton.B))
        {
            toggleMesh();
        }

        if (controllerInput.GetButtonDown(ControllerButton.X))
        {
            resetExperiment();
        }

        if (controllerInput.GetButtonDown(ControllerButton.A))
        {
            nextExperimentStep();
        }

        if (controllerInput.GetButton(ControllerButton.DPadUp))
        {
            translateUp();
        }

        if (controllerInput.GetButton(ControllerButton.DPadDown))
        {
            translateDown();
        }

        if (controllerInput.GetButton(ControllerButton.DPadRight))
        {
            translateRight();
        }

        if (controllerInput.GetButton(ControllerButton.DPadLeft))
        {
            translateLeft();
        }

        if (controllerInput.GetButton(ControllerButton.LeftShoulder))
        {
            translatePull();
        }

        if (controllerInput.GetButton(ControllerButton.RightShoulder))
        {
            translatePush();
        }

        if (controllerInput.GetButton(ControllerButton.LeftThumbstick))
        {
            yawRotateClock();
        }

        if (controllerInput.GetButton(ControllerButton.RightThumbstick))
        {
            yawRotateCntrClock();
        }

        if (controllerInput.GetButtonDown(ControllerButton.View))
        {
            quitApplication();
        }
    }

    private void quitApplication()
    {
        Application.Quit();
    }

    private void setActiveExperiment()
    {
        isMarkerExperiment = !isMarkerExperiment;
        markerPlacementScript.changeActiveState();
        abdominalIncisionScript.changeActiveState();
    }

    private void nextExperimentStep()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.Advance();
        }
        else
        {
            abdominalIncisionScript.Advance();
        }
    }

    private void toggleMesh()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.ToggleMannequinMesh();
        }
        else
        {
            abdominalIncisionScript.ToggleMannequinMesh();
        }
    }

    private void resetExperiment()
    {
        if (isMarkerExperiment)
        {
            markerPlacementScript.ResetExperiment();
        }
        else
        {
            abdominalIncisionScript.ResetExperiment();
        }
    }

    private void translateUp()
    {
        markerPlacementScript.TranslateModel(0.0f, transAmount, 0.0f);
        abdominalIncisionScript.TranslateModel(0.0f, transAmount, 0.0f);
    }

    private void translateDown()
    {
        markerPlacementScript.TranslateModel(0.0f, -transAmount, 0.0f);
        abdominalIncisionScript.TranslateModel(0.0f, -transAmount, 0.0f);
    }

    private void translateRight()
    {
        markerPlacementScript.TranslateModel(transAmount, 0.0f, 0.0f);
        abdominalIncisionScript.TranslateModel(transAmount, 0.0f, 0.0f);
    }

    private void translateLeft()
    {
        markerPlacementScript.TranslateModel(-transAmount, 0.0f, 0.0f);
        abdominalIncisionScript.TranslateModel(-transAmount, 0.0f, 0.0f);
    }

    private void translatePull()
    {
        markerPlacementScript.TranslateModel(0.0f, 0.0f, -transAmount);
        abdominalIncisionScript.TranslateModel(0.0f, 0.0f, -transAmount);
    }

    private void translatePush()
    {
        markerPlacementScript.TranslateModel(0.0f, 0.0f, transAmount);
        abdominalIncisionScript.TranslateModel(0.0f, 0.0f, transAmount);
    }

    private void yawRotateClock()
    {
        markerPlacementScript.RotateModel(rotAmount);
        abdominalIncisionScript.RotateModel(rotAmount);
    }

    private void yawRotateCntrClock()
    {
        markerPlacementScript.RotateModel(-rotAmount);
        abdominalIncisionScript.RotateModel(-rotAmount);
    }
}
