﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.VR.WSA;

public class AbdominalIncision : MonoBehaviour
{
    public bool isActive = false;

    private int numStages = 18;
    private int currentStage;

    private float transAmount = 0.025f;
    private float rotAmount = 2.5f;

    private GameObject mainContainer;
    private Transform MannequinMeshContainer;
    private List<GameObject> stagesContainer;

    // Use this for initialization
    void Start ()
    {
        if (mainContainer == null)
        {
            mainContainer = GameObject.Find("AbdominalIncisionExperiment");
            if (mainContainer == null)
            {
                Debug.LogError("could not find root model");
            }
        }

        if (MannequinMeshContainer == null)
        {
            MannequinMeshContainer = transform.Find("Mannequin Abdominal");
            if (MannequinMeshContainer == null)
            {
                Debug.LogError("could not find mannequin mesh");
            }
        }

        stagesContainer = new List<GameObject>();
        Transform stages = transform.Find("Stages");

        for (int stageId = 0; stageId <= numStages; stageId++)
        {
            Transform currentStage = stages.Find("Stage " + stageId);
            stagesContainer.Add(currentStage.gameObject);
        }

        HideMannequinMesh();

        ResetExperiment();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Switch active model
        if (Input.GetKeyDown(KeyCode.S))
        {
            changeActiveState();
        }
        if (isActive)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Advance();
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                ResetExperiment();
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                ToggleMannequinMesh();
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                TranslateModel(0.0f, transAmount, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                TranslateModel(0.0f, -transAmount, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                TranslateModel(transAmount, 0.0f, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                TranslateModel(-transAmount, 0.0f, 0.0f);
            }
            else if (Input.GetKeyDown(KeyCode.P))//Push
            {
                TranslateModel(0.0f, 0.0f, transAmount);
            }
            else if (Input.GetKeyDown(KeyCode.L))//Pull
            {
                TranslateModel(0.0f, 0.0f, -transAmount);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                RotateModel(rotAmount);
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                RotateModel(-rotAmount);
            }
        }

        updateFocus();
    }

    public void changeActiveState()
    {
        if (isActive)
        {
            HideMannequinMesh();

            ResetExperiment();
        }
        isActive = !isActive;
    }

    public void ToggleMannequinMesh()
    {
        MannequinMeshContainer.gameObject.SetActive(!MannequinMeshContainer.gameObject.activeSelf);
    }

    public void TranslateModel(float transX, float transY, float transZ)
    {
        Transform mainContainerTransform = mainContainer.GetComponent<Transform>();
        mainContainerTransform.position = new Vector3(mainContainerTransform.position.x + transX, mainContainerTransform.position.y + transY, mainContainerTransform.position.z + transZ);
    }

    public void RotateModel(float angle)
    {
        Transform mainContainerTransform = mainContainer.GetComponent<Transform>();
        mainContainerTransform.Rotate(0.0f, angle, 0.0f);
    }

    void HideMannequinMesh()
    {
        MannequinMeshContainer.gameObject.SetActive(false);
    }

    public void ResetExperiment()
    {
        InitTrial(0);
    }

    public void InitTrial(int stageNumber)
    {
        currentStage = stageNumber;

        // hide all other trials
        for (int trialIdx = 0; trialIdx <= numStages; trialIdx++)
        {
            GameObject trialContainer = stagesContainer[trialIdx];
            trialContainer.SetActive(trialIdx == currentStage);
        }
    }

    public void Advance()
    {
        if ((currentStage+1)<=numStages)
        {
            InitTrial(currentStage + 1);
        }
        else
        {
            InitTrial(0);
        }
    }

    private void updateFocus()
    {
        var normal = -Camera.main.transform.forward;

        for (int stageId = 0; stageId <= numStages; stageId++)
        {
            GameObject focusedObjectStage = stagesContainer[stageId];
            var position = focusedObjectStage.transform.position;
            HolographicSettings.SetFocusPointForFrame(position, normal);
        }
    }
}
